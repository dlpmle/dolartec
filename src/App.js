import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink
} from "react-router-dom";
import Content from "./components/Content.jsx";
import Faq from "./components/Faq.jsx";
import Home from './components/Home.jsx';
import Us from './components/Us';

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/nosotros">
            <Us />
          </Route>
          <Route path="/contenido">
            <Content />
          </Route>
          <Route path="/preguntas-frecuentes">
            <Faq />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
