import React from 'react'
/* import register from '../assets/images/register.jpg'; */
/* import register from '../assets/images/register-1x.jpg'; */
import register from '../assets/images/register.png';
import transfer from '../assets/images/transfer.png';
import done from '../assets/images/done.png';
import play from '../assets/images/play-circle.png'
import bot from '../assets/images/BOT.png';
import BtnRegister from './BtnRegister';

const HowToFunctionSection = () => {
    return (
        <div className="howtofunction">
            <div className="container">
                <div className="row row-howtofunction">
                    <div className="title-howtofunction mt-5">
                        <h2 className="mt-5">¿Cómo funciona?</h2>
                        {/* <h3>Ver tutorial <u>aquí</u>&nbsp;&nbsp;<img src={play} alt="Play" /></h3> */}
                    </div>
                </div>
                <div className="row row-howtofunction">
                    <div className="pt-5">
                        <div className="text-center">
                            <img src={register} className="img-register" alt="Register"/>
                            <div className="card-body">
                                <h5 className="card-title">Regístrate</h5>
                                <p className="card-text text-center">Inicia sesión y completa los datos solicitados.</p>
                            </div>
                        </div>
                    </div>
                    <div className="pt-5">
                        <div className="text-center">
                            <img src={transfer} className="img-transfer" alt="Transfer"/>
                            <div className="card-body">
                                <h5 className="card-title">Transfiere</h5>
                                <p className="card-text text-center">Envíanos la constancia de tu operación.</p>
                            </div>
                        </div>
                    </div>
                    <div className="pt-5">
                        <div className="text-center">
                            <img src={done} className="img-done" alt="Done"/>
                            <div className="card-body">
                                <h5 className="card-title">¡Listo!</h5>
                                <p className="card-text text-center">Recibe el cambio en tu cuenta bancaria.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row row-btn-register-howtofunction">
                    <div className="mt-5">
                        <BtnRegister block={true} />
                    </div>
                </div>    
                <div className="bot d-flex">
                    <div></div>
                    <div className="ml-auto">
                        <img src={bot} alt="Bot" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HowToFunctionSection
