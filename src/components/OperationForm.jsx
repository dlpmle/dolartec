import React from 'react'
import arrow from '../assets/images/arrow.png';

const OperationForm = (props) => {
    const isBtnOperation = props.isBtnOperation ? "btn btn-lg btn-block btn-operation" : "btn btn-lg btn-block";
    const classTop = props.backgroundAzulMarino ? "card card-operation background-azul-marino" : "card card-operation";
    const classAddonButton= props.isAddonColorBlue ? "input-group-text background-blue" : "input-group-text";
    return (
        <>
            <div className={classTop}>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item li-card-one">
                        <div className="row-operationform">
                            <div className="">
                                Dólar compra: <span>3,596</span>
                            </div>
                            <div className="">
                                Dólar venta: <span>3,345</span>
                            </div>
                        </div>
                    </li>
                    <li className="list-group-item li-card-two">
                        <div className="input-group mb-3">
                            <div className="">
                                <span className={classAddonButton} id="basic-addon1">USD</span>
                            </div>
                            <input type="text" className="form-control" id="send" placeholder="Tú envías" aria-label="Username" aria-describedby="basic-addon1"/>
                        </div>
                        <div className="input-group mb-3">
                            <div className="">
                                <span className={classAddonButton} id="basic-addon2">PEN</span>
                            </div>
                            <input type="text" className="form-control" id="received" placeholder="Tú recibes" aria-label="Username" aria-describedby="basic-addon2"/>
                        </div>
                        
                        <div className="blue-circle">
                            <div className="white-circle">
                                <img src={arrow} alt="Flecha" className="arrow"/>
                                {/* <div className="inner-arrow-circle"></div> */}
                            </div>
                        </div>
                    </li>
                    <li className="list-group-item li-card-three">
                        <a href="/" className={isBtnOperation}>Iniciar operación</a>
                    </li>
                </ul>
            </div> 
        </>
    )
}

export default OperationForm
