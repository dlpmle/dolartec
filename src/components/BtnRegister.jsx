import React from 'react'

const BtnRegister = (props) => {
    const block = props.block ? "btn btn-register btn-block mb-5" : "btn btn-register mb-5";
    return (
        <>
            <button className={block}>Regístrate</button>            
        </>
    )
}

export default BtnRegister
