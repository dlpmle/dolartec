import React from 'react'
import ContentUs from './ContentUs'
import Footer from './Footer'
import Navbar from './Navbar'

const Us = () => {
    return (
        <div>
            <Navbar />
                <ContentUs />
            <Footer />
        </div>
    )
}

export default Us
