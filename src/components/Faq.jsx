import React from 'react'
import Footer from './Footer'
import Navbar from './Navbar'
import saly from '../assets/images/Saly-17-1x-cut.png'
import BtnRegister from './BtnRegister'
import BtnArrowLeft from './BtnArrowLeft'

const Faq = () => {
    return (
        <>
        <Navbar />
        <div className="faq">
            <div className="container">
                <div className="row faq-row">
                    <div className="col-md-6 mt-5">
                        <div className="faq-title">Preguntas Frecuentes</div>
                        <div className="faq-description">En caso no encuentres respuesta a tus dudas, te invitamos a ponerte en contacto con nosotros.</div>
                    </div>
                    <div className="col-md-6">
                        <img src={saly} alt="Saly" />
                    </div>
                </div>
                <div className="faq-questions mt-5">
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>                       
                        ¿Quiénes somos?
                    </div>
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>
                        ¿Cómo funciona?
                    </div>
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>
                        ¿Cuál es el horario de atención?
                    </div>
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>
                        ¿Cómo me garantizan seguridad?
                    </div>
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>
                        ¿Cómo creo una cuenta?
                    </div>
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>
                        ¿Cual es el procedimiento de cambio?
                    </div>
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>
                        ¿Hasta qué monto puedo cambiar en Dolartec?
                    </div>
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>
                        ¿Hay algún monto mínimo por transacción?
                    </div>
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>
                        ¿Puedo cambiar mi tarjeta de crédito en Dolartec?
                    </div>
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>
                        ¿Cuanto demora mi pago de tarjeta de crédito?
                    </div>
                    <div className="faq-question">
                        <div className="arrow-btn-container">
                            <BtnArrowLeft />
                        </div>
                        ¿Cómo cancelar mi operación?
                    </div>
                </div>
                <div className="row faq-register-btn-row mt-5">
                    <BtnRegister />
                </div> 
            </div>
        </div>
        <Footer />
        </>
    )
}

export default Faq
