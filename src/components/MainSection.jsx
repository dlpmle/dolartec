import React from 'react'
import style from '../assets/css/index.css';
import hand from '../assets/images/hand.png';
import certified from '../assets/images/certified.png';
import promo from '../assets/images/promo.png';
import OperationForm from './OperationForm';

const MainSection = () => {

    return (
        <div className="init-section">
            <div className="container">
                <div className="row row-1 business-hours-text p-5">
                    <div className="col-md-8 offset-md-2">
                        {/* <h6 className="m-5 text-center">
                            <span className="badge badge-secondary badge-schedule">Horario de atención: L-V de 9:00 am a 6:00 pm y Sábados de 9:00 am a 1:00 pm</span>
                        </h6> */}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8">
                        <div className="card card-marketing">
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item li-card-one">Tu casa de <span>cambio digital</span>, con el mejor tipo de cambio actual.</li>
                                <li className="list-group-item li-card-two">Rápido, Fácil, Seguro, y al mejor precio online garantizado.</li>
                                <li className="list-group-item li-card-two"><span className="badge badge-secondary badge-schedule">Horario de atención: L-V de 9:00 am a 6:00 pm y Sábados de 9:00 am a 1:00 pm</span></li>
                                <li className="list-group-item li-card-three">
                                    <div className="row row-li-card-three">
                                        <div className="">
                                            <img src={certified} alt="Info" />&nbsp;
                                        </div>
                                        <div className="certified">
                                            <span>Registrados en la SBS</span>
                                        </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <div className="">
                                            <img src={promo} alt="Info" />&nbsp;
                                        </div>
                                        <div className="promo">
                                            <span>Promociones</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <OperationForm isBtnOperation={true} background="blue" isAddonColorBlue={false}/>
                    </div>
                </div>
                <img src={hand} alt="No se pudo cargar la imagen" className="img-fluid space-img-hand"/>
            </div>
            
        </div>
    )
}

export default MainSection
