import React from 'react'
import video from '../assets/images/video.png';
import circle from '../assets/images/circle.png';
import hand from '../assets/images/Point_right.png';
import play from '../assets/images/play.png';
import vector from '../assets/images/vector-videosection.png';
import points_background from '../assets/images/points-videosection.png';

const VideoSection = () => {
    return (
        <div className="videosection">
            <div className="container">
                <div className="row videosection-row">
                    <div className="col-md-6 mt-5 text-videosection">
                        <div className="text-videosection-title">Cambiar dólares nunca fue tan <span>fácil</span></div>
                        <div className="text-videosection-subtitle">Preparamos un video que te servirá de guía</div>
                    </div>
                    <div className="col-md-6 mt-5">
                        <div className="video-screen">
                            <img src={video} alt="Pantalla video" />
                        </div>
                    </div>
                    <div className="img-circle">
                        <img src={circle} alt="Circle" className=""/>
                    </div>
                    <div className="img-play">
                        <img src={play} alt="Play" className=""/>
                    </div>
                    <div className="point-right">
                        <img src={hand} alt="Mano señalando" />
                    </div>
                </div>
            </div>
            <div className="vector-1">
                <img src={vector} alt="Vector" />
            </div>
            <div className="points-vector">
                <img src={points_background} alt="Points VideSection" />
            </div>
        </div>
        
    )
}

export default VideoSection
