import React from 'react'
import Footer from './Footer'
import Navbar from './Navbar'
import OperationForm from './OperationForm'
import pre_dolar from '../assets/images/pre-dolar.png';
import BtnRegister from './BtnRegister';

const Content = () => {
    return (
        <>
        <Navbar />
        <div className="content">
            <div className="container">
                <div className="row">
                    <div className="col-md-7 mt-5 mb-5">
                        <div className="title-content">Contenido para <span>potenciar</span> tus finanzas</div>
                        <div className="subtitle-content">Queremos ayudarte a alcanzar tus metas finacieras <span>!Tú puedes!</span></div>
                    </div>
                    <div className="col-md-5 mt-5 mb-5">
                        <OperationForm isBtnOperation={false} backgroundAzulMarino={true} isAddonColorBlue={true}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 mb-4">
                        <div className="title-row-content">Lo más visto</div>
                    </div>
                </div>
                <div className="row row-content mb-3">
                    <div className="col-md-4">
                        <img src={pre_dolar} alt="Prevista del dolar" className="pre-dolar-img"/>
                    </div>
                    <div className="col-md-4">
                        <img src={pre_dolar} alt="Prevista del dolar" className="pre-dolar-img"/>
                    </div>
                    <div className="col-md-4">
                        <img src={pre_dolar} alt="Prevista del dolar" className="pre-dolar-img"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 mb-4">
                        <div className="title-row-content">Finanzas comerciales</div>
                    </div>
                </div>
                <div className="row row-content mb-3">
                    <div className="col-md-4">
                        <img src={pre_dolar} alt="Prevista del dolar" className="pre-dolar-img"/>
                    </div>
                    <div className="col-md-4">
                        <img src={pre_dolar} alt="Prevista del dolar" className="pre-dolar-img"/>
                    </div>
                    <div className="col-md-4">
                        <img src={pre_dolar} alt="Prevista del dolar" className="pre-dolar-img"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 mb-4">
                        <div className="title-row-content">Finanzas personales</div>
                    </div>
                </div>
                <div className="row row-content">
                    <div className="col-md-4 mb-5">
                        <img src={pre_dolar} alt="Prevista del dolar" className="pre-dolar-img"/>
                    </div>
                    <div className="col-md-4 mb-5">
                        <img src={pre_dolar} alt="Prevista del dolar" className="pre-dolar-img"/>
                    </div>
                    <div className="col-md-4 mb-5">
                        <img src={pre_dolar} alt="Prevista del dolar" className="pre-dolar-img"/>
                    </div>
                </div>
                <div className="row row-content-btn">
                    <div className="col-md-6 col-content-btn">
                        <BtnRegister />
                    </div>                    
                </div>
            </div>
        </div>
        <Footer />
        </>
    )
}

export default Content
