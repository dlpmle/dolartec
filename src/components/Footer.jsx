import React from 'react'
/* import logo_footer from '../assets/images/logo-dolartec.svg'; */
import logo_footer from '../assets/images/logo-footer.png';
import libro_reclamaciones from '../assets/images/libro-reclamaciones.png';
import libro_sugerencias from '../assets/images/libro-sugerencias.png';
import facebook_icon from '../assets/images/facebook-icon.png';
import linkedin_icon from '../assets/images/linkedin-icon.png';
import youtube_icon from '../assets/images/youtube-icon.png';
import instagram_icon from '../assets/images/instagram-icon.png';
import libro from '../assets/images/libro.png';

const Footer = () => {
    return (
        <div className="footer">
            <div className="container container-footer">
                <div className="row">
                    <div className="col-md-2 mt-5">
                        <img src={logo_footer} alt="Logo Dollartec" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4 mt-4">
                        <div className="footer-section-title mb-2">
                            HORARIOS DE ATENCIÓN
                        </div>
                        <div className="footer-section-text">
                            Horario regular: Lunes a Viernes: 9:00 am a 6:00 pm
                        </div>
                        <div className="footer-section-text mb-3">
                            Sábados 9:00 am a 1:00 pm
                        </div>
                        <div className="footer-section-title mb-2">
                            CONTACTO
                        </div>
                        <div className="footer-section-text">
                            info@dolartec.com
                        </div>
                        <div className="footer-section-text">
                            997 030 884
                        </div>
                    </div>
                    <div className="col-md-4 mt-4">
                        <div className="footer-section-title mb-2">
                            Dolartec Fintech
                        </div>
                        <div className="footer-section-text mb-3">
                            20605653244
                        </div>
                        <div className="footer-section-title mb-2">
                            Registro SBS
                        </div>
                        <div className="footer-section-text">
                            1382-2021
                        </div>
                        <div className="footer-section-text mb-3">
                            Av. José Pardo 434, Centro Empresarial Lit One - Piso 16, Miraflores - Lima, Perú
                        </div>
                    </div>
                    <div className="col-md-4 col-books-footer">
                        <div className="son">
                            <img src={libro_reclamaciones} alt="Libro de reclamaciones" />
                        </div>
                        <div className="book-img-footer-1">
                            <img src={libro} alt="Libro" />
                        </div>
                        <div className="book-text-footer-1">
                            Libro de reclamaciones
                        </div>
                        <div className="son">
                            <img src={libro_sugerencias} alt="Libro de sugerencias" />
                        </div>
                        <div className="book-img-footer-2">
                            <img src={libro} alt="Libro" />
                        </div>
                        <div className="book-text-footer-2">
                            Libro de sugerencias
                        </div>
                    </div>
                    {/* <div className="col-md-1 col-books-footer">
                        <div className="son">
                            <img src={libro_sugerencias} alt="Libro de sugerencias" />
                        </div>
                        <div className="book-img-footer-2">
                            <img src={libro} alt="Libro" />
                        </div>
                    </div> */}
                </div>
                <hr />
                <div className="row">
                    <div className="col-md-4 pb-3">
                        <div className="footer-section-title">
                            All rights reserved ©2021 Dolartec
                        </div>
                    </div>
                    <div className="offset-md-5 col-md-2 col-social-media-icons pb-2">
                        <a href="https://www.facebook.com/Dolartec-106132131663930/">
                            <img src={facebook_icon} alt="Facebook" />
                        </a>
                        {/* <a href="https://linkedin.com">
                            <img src={linkedin_icon} alt="Linkedin" />
                        </a> */}
                        {/* <img src={youtube_icon} alt="Youtube" /> */}
                        <a href="https://instagram.com/dolartec?utm_medium=copy_link">
                            <img src={instagram_icon} alt="Instagram" />
                        </a>
                        <a href="https://vm.tiktok.com/ZMRsDPnB5/">
                            <img src={youtube_icon} alt="Youtube" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer
