import React from 'react'
import cohete from '../assets/images/digital-graph-with-businessman-hand-overlay 1.png';

const AlertSection = () => {
    return (
        <div className="alertsection">
            <div className="container alertsection-container">
                <div className="row">
                    <div className="col-md-6 mt-5">
                        <div className="alertsection-title">
                            Escoge tu mejor tipo de <span>cambio</span> y te mantendremos informado
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 mb-5">
                        <button className="btn btn-alert">Crear alerta</button>
                    </div>
                </div>
                <div className="alertsection-img">
                    <img src={cohete} alt="Cohete" className="img-fluid"/>
                </div>
            </div>
        </div>
    )
}

export default AlertSection
