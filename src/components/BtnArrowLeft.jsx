import React from 'react'
import arrow_left from '../assets/images/arrow-left.png';

const BtnArrowLeft = () => {
    return (
        <div className="btn-arrow-left">
            <img src={arrow_left} alt="Flecha a la derecha" />
        </div>
    )
}

export default BtnArrowLeft
