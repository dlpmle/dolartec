import React from 'react'
import OpinionCard from './OpinionCard'
import photo1 from '../assets/images/avatar1.png';
import photo2 from '../assets/images/avatar2.png';
import hand_left from '../assets/images/hand-left.png';
import hand_right from '../assets/images/hand-right.png';
import instagram_opinion from '../assets/images/instagram-opinion.png';
import facebook_opinion from '../assets/images/facebook-opinion.png';

const OpinionSection = () => {
    return (
        <div className="opinionsection">
            <div className="container container-opinion">
                <div className="row opinion-row">
                    <div className="opinionsection-title mt-4 mb-4">Conoce lo que opinan de nosotros</div>
                </div>
                <div className="row opinion-row">
                    <div className="opinion-scroll">
                        <OpinionCard photo={photo1} socialPhoto={instagram_opinion} title="Alonso Molina compro dólares" time="Hace 1 hora" text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rhoncus est nisl, sed laoreet magna vestibulum et. Ut tristique tristique purus, eu pretium nisl gravida vitae. Donec congue nec nisl sollicitudin ullamcorper. Donec tempor est nec euismod porttitor. Nam viverra varius mollis."/>
                        <OpinionCard photo={photo2} socialPhoto={facebook_opinion} title="Alison Quintana compró dólares" time="Hace 2 días" text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rhoncus est nisl, sed laoreet magna vestibulum et. Ut tristique tristique purus, eu pretium nisl gravida vitae. Donec congue nec nisl sollicitudin ullamcorper. Donec tempor est nec euismod porttitor. Nam viverra varius mollis." />
                        <OpinionCard photo={photo2} socialPhoto={instagram_opinion} title="Alison Quintana compró dólares" time="Hace 2 días" text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rhoncus est nisl, sed laoreet magna vestibulum et. Ut tristique tristique purus, eu pretium nisl gravida vitae. Donec congue nec nisl sollicitudin ullamcorper. Donec tempor est nec euismod porttitor. Nam viverra varius mollis." />
                    </div>
                </div>
                
            </div>
            <div className="hand-left-opinion">
                <img src={hand_left} alt="Mano izquierda" />
            </div>
            <div className="hand-right-opinion">
                <img src={hand_right} alt="Mano derecha" />
            </div>
        </div>
    )
}

export default OpinionSection
