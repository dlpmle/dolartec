import React from "react";

const OpinionCard = (props) => {
  return (
    <div className="opinion-card mb-2">
      <div className="row pt-3 pb-3">
        <div className="col-md-2 m-3">
          <div className="social-photo-opinion">
            <img src={props.socialPhoto} alt="Red Social" />
          </div>
          <img src={props.photo} className="rounded-circle" alt="Foto 1" />
        </div>
        <div className="col-md-9">
          <div className="subject-opinion">{props.title}</div>
          <div className="time-opinion">{props.time}</div>
          <div className="text-opinion">{props.text}</div>
        </div>
      </div>
    </div>
  );
};

export default OpinionCard;
