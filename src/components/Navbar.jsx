import React from 'react'
import logo from '../assets/images/logo-dolartec.svg';
import style from '../assets/css/index.css';
import { Link } from 'react-router-dom';
import MobileMenu from './MobileMenu';

const Navbar = () => {
    const [classMenu, setClassMenu] = React.useState("hide");
    const showMenu = () => {
        setClassMenu("show");
    }

    const hideMenu = () => {
        setClassMenu("hide");
    }

    return (
        <>
            <MobileMenu className={classMenu} fun={hideMenu}/>
            <nav className="navbar navbar-expand-lg navbar-light">
                <div className="container">
                    <Link className="navbar-brand" to="/">
                        <img src={logo} className="brand" alt="No funciono" />
                    </Link>
                    <button className="navbar-toggler" onClick={ () => showMenu() } type="button" data-toggle="collapse"  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        {/* <span class="navbar-toggler-icon"></span> */}
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                            <path stroke="#D6E7EE" strokeLinecap="round" strokeMiterlimit="10" strokeWidth="2" d="M4 7h22M4 15h22M4 23h22"/>
                        </svg>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="nav navbar-nav navbar-logo">
                            <li className="nav-item active">
                                <Link className="nav-link" to="/nosotros">
                                    Nosotros <span className="sr-only">(current)</span>
                                </Link>
                            </li>
                            <li className="nav-item active">
                                <Link className="nav-link" to="/contenido">Contenido <span className="sr-only">(current)</span></Link>
                            </li>
                            <li className="nav-item active">
                                <Link className="nav-link" to="/preguntas-frecuentes">¿Tienes dudas? <span className="sr-only">(current)</span></Link>
                            </li>
                            <li className="nav-item active">
                                <a className="nav-link" href="#">Iniciar sesión <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item active">
                                <a className="btn btn-navbar">Regístrate</a>
                            </li>
                        </ul>
                    </div>
                </div>
                
            </nav>
            </>
        )
}

export default Navbar
