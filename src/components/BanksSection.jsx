import React from 'react'
import bcp from '../assets/images/bcp.png';
import interbank from '../assets/images/interbank.png';
import bbva from '../assets/images/bbva.png';
import banbif from '../assets/images/banbif.png';
import scotiabank from '../assets/images/scotiabank.png';
import pichincha from '../assets/images/pichincha.png';

const BanksSection = () => {
    return (
        <div className="container mt-3 mb-3">
            <div className="row row-bankssection">
                <div className="text-bank">Operaciones inmediatas</div>
                <div className="">
                    <img src={bcp} alt="BCP logo" />
                </div>
                <div className="text-bank">Operaciones interbancarias</div>
                <div className="">
                    <img src={interbank} alt="Interbank logo" />
                </div>
                <div className="">
                    <img src={bbva} alt="BBVA logo" />
                </div>
                <div className="">
                    <img src={scotiabank} alt="Scotiabank logo" />
                </div>
                <div className="">
                    <img src={banbif} alt="Banbif logo" />
                </div>
                <div className="">
                    <img src={pichincha} alt="Pichincha logo" />
                </div>
            </div>
        </div>
    )
}

export default BanksSection
