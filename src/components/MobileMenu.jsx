import React from 'react'
import logo from '../assets/images/mobile-logo.png';
import x_button from '../assets/images/x-button.png';
import BtnRegister from './BtnRegister';
import { Link } from 'react-router-dom';

const MobileMenu = (props) => {
    const classMenu = `mobilemenu ${props.className}`;
    const hideMenu = () => {
        props.fun();
    }

    return (
        <div className={classMenu}>
            <div className="container">
                <div className="row">
                    <div className="col-md-12 mt-5 text-right">
                        <img src={x_button} alt="Boton X" onClick={ () => { hideMenu() } }/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 mt-5 mb-5 text-center">
                        <img src={logo} alt="Logo Dolartec" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 mb-3 text-center text-menu">
                        <Link className="text-menu" to="/nosotros">Nosotros</Link>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 mb-3 text-center text-menu">
                        <Link className="text-menu" to="/contenido">Contenido</Link>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 mb-3 text-center text-menu">
                        <Link className="text-menu" to="/preguntas-frecuentes">¿Tienes dudas?</Link>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 mb-3 text-center text-menu">
                        <Link className="text-menu" to="/nosotros">Iniciar Sesión</Link>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 mb-5 text-center text-menu">
                        <Link className="text-menu" to="/"><BtnRegister /></Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MobileMenu
