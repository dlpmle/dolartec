import React from 'react'
/* import man_laptop from '../assets/images/man-laptop.png'; */
import man_laptop from '../assets/images/man-laptop-2.png';
import man_movil from '../assets/images/man-movil.png';
import man_money from '../assets/images/man-money.png';

const WhyChooseSection = () => {
    return (
        <div className="whychoose">
            <div className="container">
                <div className="row text-center">
                    <div className="col-md-4 offset-md-4 mt-5 mb-4">
                        <div className="whychoose-title">¿Por qué elegirnos?</div>
                    </div>
                </div>
                <div className="row-cards">
                    <div className="card">
                        <div className="number-circle">1</div>
                        <div className="card-img">
                            <img className="card-img-left img-fluid" src={man_laptop} alt="Card image cap" />
                        </div>
                        <div className="card-body">
                            <h5 className="card-title text-center mb-0">100% digitales</h5>
                            <p className="card-text text-center">Obten los dólares que necesitas en tu cuenta de manera 100% digital.</p>
                        </div>
                    </div>
                    <div className="card">
                        <div className="number-circle">2</div>
                        <div className="card-img">
                            <img className="card-img-center img-fluid" src={man_movil} alt="Card image cap" />
                        </div>
                        <div className="card-body">
                            <h5 className="card-title text-center mb-0">Seguridad</h5>
                            <p className="card-text text-center">Registrados en la SBS y certificados con seguridad que cifran tu información.</p>
                        </div>
                    </div>
                    <div className="card">
                        <div className="number-circle">3</div>
                        <div className="card-img">
                            <img className="card-img-right img-fluid" src={man_money} alt="Card image cap" />
                        </div>
                        <div className="card-body">
                            <h5 className="card-title text-center mb-0">Ahorra más</h5>
                            <p className="card-text text-center">Te ayudamos a ahorrar porque te ofrecemos el mejor tipo de cambio.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WhyChooseSection
