import React from 'react'
import Navbar from './Navbar';
import style from '../assets/css/index.css';
import MainSection from './MainSection';
import BanksSection from './BanksSection';
import HowToFunctionSection from './HowToFunctionSection';
import VideoSection from './VideoSection';
import WhyChooseSection from './WhyChooseSection';
import AlertSection from './AlertSection';
import OpinionSection from './OpinionSection';
import Footer from './Footer';
import MobileMenu from './MobileMenu';

const Home = () => {
    return (
        <div>
            <Navbar/>
            <MainSection/>
            <BanksSection />
            <HowToFunctionSection/>
            <VideoSection />
            <WhyChooseSection />
            <AlertSection />
            <OpinionSection />
            <div className="arc"></div>
            <Footer />       
        </div>
    )
}

export default Home
