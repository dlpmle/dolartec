import React from 'react'
import puzzle from '../assets/images/puzzle.png';
import BtnRegister from './BtnRegister';

const ContentUs = () => {
    return (
        <div className="contentus">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 mt-5 mb-5">
                        <img src={puzzle} alt="Rompecabezas" />
                    </div>
                    <div className="col-md-6 mt-5 mb-5">
                        <div className="title-contentus">¿Quiénes somos?</div>
                        <div className="text-contentus">
                            <p>
                                Somos expertos en finanzas inscritos en el Registro de Empresas y Personas que efectuan Operaciones Financieras o de Cambio de Moneda SBS, Resolución N° 1382-2021 en Perú.
                            </p>
                            <p>
                                Buscamos mejorar la forma en que se realizan las operaciones de cambio de moneda en línea, con la finalidad de que todos nuestros usuarios ahorren cambiando dólares desde donde estén con el mejor tipo de cambio actual .
                            </p>
                        </div>
                        <div className="button-section-contentus">
                            <BtnRegister block={false} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ContentUs
